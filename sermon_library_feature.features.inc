<?php
/**
 * @file
 * sermon_library_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sermon_library_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "panels" && $api == "layouts") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function sermon_library_feature_node_info() {
  $items = array(
    'sermon' => array(
      'name' => t('Sermon'),
      'base' => 'node_content',
      'description' => t('A Sermon Content Type'),
      'has_title' => '1',
      'title_label' => t('Sermon Title'),
      'help' => t('Edit the information here to insert a new sermon into your resource library.'),
    ),
  );
  return $items;
}

/**
 * Implements hook_views_api().
 */
function sermon_library_feature_views_api() {
  return array(
    'api' => '3.0-alpha1',
  );
}
