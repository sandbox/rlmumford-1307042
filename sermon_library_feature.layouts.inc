<?php
/**
 * @file
 * sermon_library_feature.layouts.inc
 */

/**
 * Implements hook_default_panels_layout().
 */
function sermon_library_feature_default_panels_layout() {
  $export = array();

  $layout = new stdClass;
  $layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
  $layout->api_version = 1;
  $layout->name = 'two_column_9_3';
  $layout->admin_title = 'Two Column 9-3';
  $layout->admin_description = '';
  $layout->category = 'Column: 2';
  $layout->plugin = 'flexible';
  $layout->settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'main_',
          1 => 'right_sidebar',
        ),
        'parent' => 'main',
      ),
      'main_' => array(
        'type' => 'region',
        'title' => 'Main',
        'width' => '75.02805836139169',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      'right_sidebar' => array(
        'type' => 'region',
        'title' => 'Right Sidebar',
        'width' => '24.97194163860831',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
    ),
  );
  $export['two_column_9_3'] = $layout;

  return $export;
}
